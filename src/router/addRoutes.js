import router from "./index";
const firstLetterUpperCase = (str) => {
  return str.replace(/\b\w+\b/g, (word) => {
    return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
  });
};
export default function (menus) {
  menus.map((item) => {
    if (item.children?.length > 0) {
      item.children.map((child) => {
        router.addRoute("Home", {
          path: child.path,
          name: firstLetterUpperCase(child.path),
          component: () =>
            import(`../views/${firstLetterUpperCase(child.path)}.vue`),
        });
      });
    }
  });
}
