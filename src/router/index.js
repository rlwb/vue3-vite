import { createRouter, createWebHistory } from "vue-router";
import store from "../store";
import addRoutes from "./addRoutes";
const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
let registerRouteFresh = true;
router.beforeEach(async (to, from) => {
  if (to.name == "Login") {
    return true;
  }
  if (!store.state.token) {
    return {
      name: "Login",
    };
  }
  if (registerRouteFresh) {
    await store.dispatch("getMenus");
    addRoutes(store.state.menus);
    registerRouteFresh = false;
    return {
      ...to,
      replace: true,
    };
  }
  return true;
});

export default router;
