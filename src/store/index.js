import { createStore } from "vuex";
import http from "../main";
const store = createStore({
  state() {
    return {
      token: localStorage.getItem("token") || "",
      username: localStorage.getItem("username") || "",
      menus: [],
    };
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
      localStorage.setItem("token", token);
    },
    SET_USERNAME(state, name) {
      state.username = name;
      localStorage.setItem("username", name);
    },
    SET_MENUS(state, menus) {
      state.menus = menus;
    },
  },
  actions: {
    getMenus({ commit }) {
      http.defaults.headers.Authorization = `${store.state.token}`;
      return http.get("menus").then(({ data: { data } }) => {
        commit("SET_MENUS", data);
        return data;
      });
    },
  },
});
export default store;
