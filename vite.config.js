import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
export default defineConfig({
  server: {
    port: 8000,
    proxy: {
      "/api": "http://116.62.246.191",
    },
  },
  plugins: [vue()],
});
